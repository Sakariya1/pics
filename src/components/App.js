import React from 'react';
import axios from 'axios';
// import unsplash from '../api/unsplash';
import SearchBar from './SearchBar';
import ImageList from './ImageList';

class App extends React.Component {

  state = { images: [] };

  // Call API using promise way::
  // onSearchSubmit(term) {
  //   axios.get('https://api.unsplash.com/search/photos', {
  //     params: {
  //       query: term
  //     },
  //     headers: {
  //       Authorization: 'Client-ID i27LDWIF4eP2bPZezyP62_JbKsSfBNMHtpHD4yc_T7w'
  //     }
  //   }).then((response) => {
  //     console.log(response)
  //   });
  // }

  // Call API using async
  onSearchSubmit = async (term) => {
    const response = await axios.
      get('https://api.unsplash.com/search/photos', {
        params: {
          query: term
        },
        headers: {
          Authorization: 'Client-ID i27LDWIF4eP2bPZezyP62_JbKsSfBNMHtpHD4yc_T7w'
        }
      });
    console.log(this);
    this.setState({ images: response.data.results });
  }

  // TODO:: this is not working
  // onSearchSubmit = async (term) => {
  //   const response = await unsplash.get('/search/photos', {
  //     params: { query: term }
  //   });
  //   this.setState({ images: response.data.results });
  // }

  render() {
    return (
      <div className="ui container" style={{ marginTop: '10px' }}>
        <SearchBar onSubmit={this.onSearchSubmit} />
        <ImageList images={this.state.images} />
        Found: {this.state.images.length} images
      </div>
    );
  }
}

export default App;
