import React from 'react';

class SearchBar extends React.Component {
  // controlled elements
  state = { term: '' };

  // arrow function if u use callback.
  // onFormSubmit = (event) => {
  //   event.preventDefault();
  //   console.log(this.state.term);
  // }

  // Call this using arrow function while on submit
  onFormSubmit(event) {
    event.preventDefault();
    console.log(this.state.term);
    this.props.onSubmit(this.state.term)
  }

  // this is uncontrolled code
  // onInputChange(event) {
  //   console.log(event.target.value);
  // }

  render() {
    return (
      <div className="ui segment">
        {/* define onFormSubmit as arrow function for binding this */}
        {/* <form onSubmit={this.onFormSubmit} className="ui form"> */}
        <form onSubmit={(event) => this.onFormSubmit(event)} className="ui form">
          <div className="field">
            <label>Image Search</label>
            <input
              type="text"
              value={this.state.term}
              onChange={(e) => this.setState({ term: e.target.value })}
            />

            {/* Allow to add upper case values in text-box */}
            {/* <input
              type="text"
              value={this.state.term}
              onChange={(e) => this.setState({ term: e.target.value.toUpperCase() })}
            /> */}

            {/* Call callback on change and make sure don't pass () while call callbacks */}
            {/* <input
              type="text"
              onChange={this.onInputChange}
            /> */}

            {/* Alternate Event Handler Syntax  */}
            {/* <input
              type="text"
              onChange={(e) => console.log(e.target.value)}
            /> */}
          </div>
        </form>
      </div>
    );
  }
}

export default SearchBar;
