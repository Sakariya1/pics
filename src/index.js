// Import the react and reactDOM libraries
import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';

// Create a react component using class
// class App extends React.Component {

//   componentDidMount() {
//   }

//   componentDidUpdate() {
//   }

//   // React says we have to define render!!
//   render() {
//     return (
//       <div className="border red">
//         fgd
//       </div>
//     )

//   }
// }

// Take the react component and show it on the screen
ReactDOM.render( <App />, document.querySelector('#root'));
